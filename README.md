# advanced-postgresql

이 article은 [Advanced PostgreSQL: A Guide](https://arctype.com/blog/postgresql-features-list/)를 편역한 것이다. 보통 RDBMS과 비교하여 Postgres의 확장된 기능에 대하여 간단한 소개를 한다.
