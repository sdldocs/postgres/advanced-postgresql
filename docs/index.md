상속, nonatomic 열, 여러분의 데이터를 보다 쉽게 저장하는 등 고급 Postgres 기능을 최대한 활용하기 위한 방법을 알아보자.

## 들어가며
NoSQL 데이터베이스를 많이 사용되고 있음에도 불구하고, 관계형 데이터베이스는 여전히 많은 응용 프로그램에서 선호되고 있다. 이것은 그들의 강력한 쿼리 능력과 신뢰성 때문이다.

관계형 데이터베이스는 데이터 구조를 자주 변경하지 않는 경우 데이터를 기반으로 복잡한 쿼리를 실행하고 검색하는 데 탁월하다. MySQL과 Postgre와 같은 오픈 소스 관계형 데이터베이스들은 Oracle, MSSQL 등과 같은 라이센스가 필요한 데이터베이스와 비교하여 안정적인 운영 등급 데이터베이스로서 비용 효율적인 대안을 제공한다. 이 문서는 확장성과 SQL 컴플라이언스에 중점을 둔 오픈 소스 데이터베이스 관리 시스템인 PostgreSQL의 고급 기능에 대한 간단한 안내서 이다.

## PostgreSQL - 일반적 사용 사례
PostgreSQL의 고유한 기능 집합을 사용하여 트랜잭션 데이터베이스 또는 데이터 웨어하우스를 구축하여 사용할 수 있다. 우수한 트랜잭션 지원과 높은 쓰기 성능으로 인해 온라인 트랜잭션 처리 워크로드를 실행하기에 이상적인 DBMS 중 하나이다. 또한, 분석과 window 기능을 포함하는 다량한 내장 기능을 통해 분석가가 통찰력을 도출하는 데 사용할 수 있는 데이터 웨어하우스로 사용할 수도 있다.

PostgreSQL는 JSON 데이터 타입과 JSON 함수를 사용하여 NoSQL 워크로드를 실행할 수 있다. PostgreSQL의 또 다른 독특한 기능은 GIS 플러그인을 통해 지리공간 데이터를 저장하고 조회를 실행할 수 있는 기능을 지원한다. PostgreSQL의 full-tewxt 기능은 복잡한 정규 표현식을 사용하지 않고 문서를 검색할 수 있도록 도와준다. 이외에도 PostgreSQL은 또한 스키마 상속, non-atomic 열, 뷰(view) 등과 같은 많은 고급 기능을 지원한다. 이러한 고급 기능에 대해 자세히 살펴보자.

## PostgreSQL - 고급 기능

### 상속(Inheritance)
PostgreSQL은 상속을 지원하여 사용자가 모델 구조를 모델링하는 테이블을 깔끔하게 설계할 수 있다. 예를 들어, 고객 테이블이 있고 office_address라는 추가 필드가 있는 특정 유형의 고객이 있는 사례를 생각해 보자. 상속을 지원하지 않는 데이터베이스에서는 아래의 DDL 문을 사용하여 두 개의 별도 테이블을 통해 처리해야 한다.

```sql
CREATE TABLE CUSTOMERS (
    name text,
    age int,
    address text
);

CREATE TABLE OFFICE_CUSTOMERS(
    name text,
    age int,
    address text,
    office_address text
);
```

PostgreSQL의 상속 기능을 이용하여 아래와 같아 DDL 문으로 처리할 수 있다. 

```sql
CREATE TABLE CUSTOMERS (
    name text,
    age int,
    address text
);
 
CREATE TABLE OFFICE_CUSTOMERS(
    office_address text
) INHERITS (customer)
```

위의 쿼리는 `CUSTOMERS` 테이블에서 상속받은 `OFFICE_CUSTOMERS` 테이블과 함께 두 개의 테이블을 생성한다.

![image_01](images/pic_01.png)

이렇게 하면 데이터베이스 성능뿐만 아니라 스키마를 갈끔하게 유지할 수 있다.

이제 몇 가지 항목을 삽입하여 작동 방식을 살펴보겠다.

```sql
INSERT INTO CUSTOMERS VALUES('ravi','32','32, head street');
INSERT INTO CUSTOMERS VALUES('michael','35','56, gotham street');
INSERT INTO OFFICE_CUSTOMERS VALUES('bane','28','56, circadia street','92 homebush');
```

첫 번째 테이블의 데이터에만 액세스해야 하는 경우 `ONLY` 키워드를 사용할 수 있다. 이 작업을 아래와 같이 수행할 수 있다.

```sql
SELECT * from ONLY CUSTOMER WHERE age > 20 ;
```

그 결과는 아래와 같다.

![image_02](images/pic_02.png)

두 테이블에서 항목을 검색해야 하는 경우 `ONLY` 키워드 없이 쿼리를 사용할 수 있다.

```sql
SELECT * from CUSTOMERS WHERE age > 20 ;
```

그 결과는 아래와 같다.

![image_03](images/pic_03.png)

### Non-Atomic 컬럼
관계형 모델의 주요 제약 조건 중 하나는 열의 값이 원자여야 한다는 것이다. PostgreSQL에는 이러한 제약 조건이 없으며 열에 쿼리를 통해 액세스할 수 있는 하위 값을 저장할 수 있다.

어떤 데이터 타입의 배열로 테이블의 필드(열)을 정의할 수 있다. 아래 문장을 사용하여 표를 작성해 보자.

```sql
CREATE TABLE customer (
   name            text,
   Address         text,
   payment_schedule  integer[],
   );
```

![image_04](images/pic_04.png)

위의 경우 `payment_schedule` 열은 정수의 배열이다. 인덱스를 지정하여 정수인 각 요소를 개별적으로 액세스할 수 있다.

아래 문장으로 몇 개의 행을 삽입한다.

```sql
INSERT INTO CUSTOMER_SCHEDULE VALUES( 'jack',
'Athens, Colarado',
'{1,2,3,4}'
)
 
INSERT INTO CUSTOMER_SCHEDULE VALUES( 'jackson',
'Tennessey, greece',
'{1,7,3,4}'
)
```

그 결과는 아래와 같다.

![image_05](images/pic_05.png)

```sql
SELECT * FROM CUSTOMER_SCHEDULE WHERE
CUSTOMER_SCHEDULE.payment_schedule[1] <> CUSTOMER_SCHEDULE.payment_schedule[2];
```

그 결과는 아래와 같다.

![image_06](images/pic_06.png)

### Window 함수
PostgreSQL window 함수는 분석 응용 프로그램에서 가장 선호하는 함수를 만드는 데 중요한 역할을 한다. window 함수는 사용자가 여러 행에 걸쳐 있는 기능을 실행하고 동일한 수의 행을 반환하는 데 도움을 준다. window 함수는 집계 후 하나의 행만 반환할 수 있다는 점에서 집계 함수와 구별된다.

직원 이름, 직원 ID, 급여 및 부서 이름이 포함된 테이블이 있다 하자.

```sql
CREATE TABLE employees (
   empno            int,
   salary         float,
   division  text
   );
```

이제 몇몇 값을 표에 삽입해 보도록 한다.

```sql
INSERT INTO employees VALUES(1,2456.7,'A')
INSERT INTO employees VALUES(2,10000.0,'A');
INSERT INTO employees VALUES(3,12000.0,'A');
INSERT INTO employees VALUES(4,2456.7,'B');
INSERT INTO employees VALUES(5,10000.0,'B');
INSERT INTO employees VALUES(6,10000.0,'C');
INSERT INTO employees VALUES(7,2456.7,'C');
```

이제 부서의 평균 급여를 직원 세부 정보와 함께 표시하려고 한다. 아래 쿼리를 사용하여 이 작업을 수행할 수 있다.

```sql
SELECT empno, salary, division,
avg(salary) OVER (PARTITION BY division) FROM EMPLOYEES;
```

그 결과는 아래와 같다.

![image_07](images/pic_07.png)

### JSON 데이터 지원
JSON 데이터를 저장하고 쿼리할 수 있는 기능을 통해 PostgreSQL를 사용하여 NoSQL 워크로드도 실행할 수 있다. 다양한 센서의 데이터를 저장하기 위해 데이터베이스를 설계하고, 모든 종류의 센서를 지원하는 데 필요한 특정 열이 정해져 있다고 확신할 수 없다고 하자. 이 경우 열 중 하나가 JSON 형식의 데이터를 저장할 수 있도록 테이블을 설계하여 비정형 또는 형식이 자주 변경되는 데이터를 저장할 수 있다.

```sql
CREATE TABLE sensor_data (
    id serial NOT NULL PRIMARY KEY,
    data JSON NOT NULL
);
```

![image_08](images/pic_08.png)

그런 다음 아래 문을 사용하여 데이터를 삽입할 수 있다.

```sql
INSERT INTO sensor_data (data)
VALUES('{ "ip": "J10.3.2.4", "payload": {"temp": "33.5","brightness": "73"}}');
```

![image_09](images/pic_09.png)

PostgreSQL을 사용하면 아래 구성을 통해 JSON 데이터의 특정 필드를 조회할 수 있다.

```sql
SELECT data->> 'ip' as ip from sensor_data;
```

![image_10](images/pic_10.png)

### Full-text 검색
PostgreSQL의 full-text 검색 기능을 사용하면 필드에 있는 단어를 기준으로 문서를 검색할 수 있다. `LIKE` 쿼리 구문을 지원하는 모든 데이터베이스가 이를 수행할 수 있어야 한다고 말할 수 있다. 하지만 PostgreSQL의 full-text 검색은 `LIKE`가 제공하는 것보다 한 단계 더 높다. 단어에 `LIKE` 쿼리를 사용하여 전체 검색을 수행하려면 정교한 정규식을 사용해야 한다. 반면, PostgreSQL의 full-text 기능은 루트 워드 또는 어휘소를 기반으로 검색할 수 있다. 어휘소를 예를 들어 설명하는 것이 가장 좋다. 문서에서 'work'라는 단어를 검색하려고 한다. work라는 단어는 'Working, works, worked' 등과 같이 여러 형태로 문서에 존재할 수 있다. PostgreSQL full-text 검색은 검색에서 이러한 모든 변화를 고려할 수 있을 정도로 지능적이다. 정규식과 `LIKE` 쿼리를 사용하여 이 작업을 수행하려고 할 경우의 쉽지 않은 문제이다. 이제 full-text 검색이 실행되는 것을 확인해 보자.

오류 로그를 저장하기 위한 테이블 로그를 작성한다고 하자.

```sql
CREATE TABLE log(
   name text,
   description text
);
```

이제 다음 문장을 사용하여 몇몇 로그에 오류 값을 삽입한다.

```sql
INSERT INTO LOG VALUES('ERROR1','Failed to retreive credentials');
INSERT INTO LOG VALUES('ERROR2','Fatal error. No records present. Please try again with a different value');
INSERT INTO LOG VALUES('ERROR3','Unable to connect. Credentials missing');
```

이제 full-text 검색을 사용하여 'miss'라는 단어가 포함된 행을 검색해 보겠다. 이를 위하여 두 가지 함수가 필요하다. `t_tsvector` 함수는 값을 어휘소로 변환하고 `t_tsquery` 함수는 일치하는 단어를 찾는다.

```sql
SELECT * FROM LOG WHERE to_tsvector(description) @@ to_tsquery('miss');
```

그 결과는 아래와 같다.

![image_11](images/pic_11.png)

우리가 검색하고자 하는 실제 단어는 'description`의 'miss'가 아니라 'missing'이었음에도 쿼리는 행을 반환할 수 있었다.

full-text 기능은 다양한 현지화 설정에서 사용할 수 있다. 그러므로 많은 언어에도 적용할 수 있다.

### Postgres Views
뷰는 특정 기준을 충족하는 가상 테이블로 생각할 수 있다. 예를 들어, 직원 테이블과 급여 테이블이 있다고 가정하자. 여기서 `id`는 공통 키이다. 워크플로우에서 이러한 표를 자주 결합하고 분석해야 하는 경우 원하는 기준으로 뷰를 정의할 수 있다. 이 뷰는 기본 테이블의 모든 변경 사항을 반영하며 결합된 테이블처럼 작동한다. 아래 DDL 문을 이용하여 테이블을 생성해 보자.

```sql
CREATE TABLE staff(
   id int,
   age int,
   address text
);
 
 
CREATE TABLE salary(
   sal_id int,
   salary int,
   division text
);
```

아래 문장을 사용하여 몇몇의 항목을 삽입한다.

```sql
INSERT INTO STAFF VALUES(1,32,'michael'); 
INSERT INTO STAFF VALUES(2,32,'sarah');

INSERT INTO SALARY VALUES(1,18000,'A');
INSERT INTO SALARY VALUES(2,32000,'B');
```

아래 문장을 사용하여 뷰를 생성한다.

```sql
CREATE VIEW STAFF_SALARY_COMBINED AS (
	SELECT * 
	FROM staff,salary 
	WHERE id=sal_id
);
```

그런 다음 일반 테이블처럼 뷰에 쿼리할 수 있다.

```sql
SELECT * FROM STAFF_SALARY_COMBINED;
```

그 결과는 아래와 같다.

![image_12](images/pic_12.png)

### Postgres에서 지리 공간(Geospatial) 데이터 처리
GIS 확장 기능이 있는 PostgreSQL을 사용하면 기하학적 좌표와 형상 정보를 표에 저장할 수 있다. 이를 특별하게 만드는 것은 거리, 면적 등을 계산하는 내장 함수를 통해 이러한 좌표에 대한 쿼리를 실행할 수 있는 기능이다. 이는 출력 또는 입력이 지리적 좌표이며 상당한 공간 처리 요구사항이 있는 사용 사례에서 PostgreSQL이 사실상 선택되어 진다. 예를 들어, 모든 도시의 기하학적 정보를 가지고 있고 도시의 면적을 알고자 하는 경우 아래 쿼리를 실행하는 것만큼 편리하다.

```sql
SELECT city.name
FROM burn_area, city
WHERE ST_Contains(burn_area.geom, city.geom)
```

결과는 제공된 좌표 조건을 충족하는 도시 이름을 출력한다.

![image_13](images/pic_13.png)

## 마치며
PostgreSQL은 관계형 데이터베이스들 중에서 거의 유일하게 많은 고급 기능을 가지고 있다. PostgreSQL를 사용하여 이러한 고유한 고급 기능을 활용하고 다른 사용자와 협업하는 것이 강력한 애플리케이션을 구축하는 기반이 된다. 

